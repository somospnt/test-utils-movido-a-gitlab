/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.test.server;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class MockRestOperationBuilderTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void withRestTemplate_withRestTemplateInstance_returnsMockRestOperationBuilder() {
        MockRestServiceServerBuilder mockRestOperation = MockRestServiceServerBuilder.withRestTemplate(restTemplate);

        assertThat(mockRestOperation).isNotNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void withRestTemplate_withRestTemplateNull_throwsIllegalArgumentException() {
        MockRestServiceServerBuilder.withRestTemplate(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withMockOperation_withAbstractMockRestOperationNull_throwsIllegalArgumentException() {
        MockRestServiceServerBuilder.withRestTemplate(restTemplate).withMockOperation(null);
    }

    @Test
    public void mock_withMockOperations_mocksMockRestOperationSequence() {
        MockRestServiceServer mockRestServiceServer = MockRestServiceServerBuilder
                .withRestTemplate(restTemplate)
                .withMockOperation(new MockRestGetOperationImpl("URL1", HttpStatus.ACCEPTED))
                .withMockOperation(new MockRestGetOperationImpl("URL2", HttpStatus.CREATED))
                .build();

        doGetRequestsTo("URL1", "URL2");

        mockRestServiceServer.verify();
    }

    @Test(expected = IllegalStateException.class)
    public void mock_withEmptyListOfMockOperations_throwsIllegalStateException() {
        MockRestServiceServerBuilder
                .withRestTemplate(restTemplate)
                .build();
    }

    private void doGetRequestsTo(String... urls) throws RestClientException {
        asList(urls).forEach(url -> restTemplate.getForEntity(url, Object.class));
    }

    public class MockRestGetOperationImpl extends AbstractMockRestOperation {

        private String url;

        @Override
        protected String getUrl() {
            return "/" + url;
        }

        @Override
        protected HttpMethod getHttpMethod() {
            return HttpMethod.GET;
        }

        @Override
        protected ResponseCreator getSuccessResponse() {
            return MockRestResponseCreators.withSuccess();
        }

        public MockRestGetOperationImpl(String url, HttpStatus httpStatus) {
            this.url = url;
            this.status = httpStatus;
        }

    }

}
